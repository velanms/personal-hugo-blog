---
author: "Velan Salis"
title: "Decluttering my online self"
date: 2023-06-09T06:18:26+05:30
description: ""
categories: []
tags: []
slug: "decluttering-my-online-self"
draft: false
---
Lately I've been noticing that my online presence has been all over the place. I usually keep my online presence organized by using a password manager and making sure I make a note of the online services that I'm a part of. Despite all that, I've been greeted with a lot of unsolicited emails, which—in all honesty, I'm tired of at this point.

So I've decided that I would declutter my online presence and organize my online self. It would require a lot of going back and forth. And my hope is eventually this exercise would make me more productive and help me use my tools better.

Good day :)
